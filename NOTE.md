Este servicio es de tipo rest y consume al otro servicio que es un soap

El Servicio tiene un endpoint de tipo get con parámetros 

Un ejemplo de la url con datos

Request

http://localhost:8080/api/employee?nombre=test&apellido=test&tipoDocumento=CC&numeroDocumento=534348&fechaNacimiento=1997-09-26&fechaVinculacion=2020-09-26&cargo=fullstack&salario=300000

Response:

{

  "name": "test",

  "lastname": "test",

  "documentType": "CC",

  "documentNumber": "534348",

  "dateOfBirth": "1997-09-26T05:00:00.000+00:00",

  "linkingDate": "2020-09-26T05:00:00.000+00:00",

  "position": "fullstack",

  "salary": 300000.0,

  "linkingTime": {

​    "years": 1,

​    "months": 2,

​    "days": 21

  },

  "currentAge": {

​    "years": 24,

​    "months": 2,

​    "days": 21

  }

}



Nota: Si el número de documento ya esta en base de datos no lo vuelve a insertar.