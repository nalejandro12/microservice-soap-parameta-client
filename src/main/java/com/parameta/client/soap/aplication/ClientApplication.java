package com.parameta.client.soap.aplication;

import com.parameta.client.soap.domain.repository.EmployeeRepository;
import com.parameta.client.soap.domain.usecase.EmployeeUseCase;
import com.parameta.client.soap.infraestructure.client.SoapClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@SpringBootApplication(scanBasePackages = "com.parameta.client.soap.infraestructure.controller")
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}


	@Bean
	public EmployeeUseCase employeeUseCase(EmployeeRepository employeeRepository){
		return new EmployeeUseCase(employeeRepository);
	}
	@Bean
	public SoapClient SoapClient(){
		return new SoapClient();
	}

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller=new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.parameta.client.soap.parameta");
		return marshaller;
	}

}
