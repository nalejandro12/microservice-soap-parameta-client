package com.parameta.client.soap.domain;

import javax.xml.datatype.XMLGregorianCalendar;

public class Employee {
    private String name;
    private String lastname;
    private String documentType;
    private String documentNumber;
    private XMLGregorianCalendar dateOfBirth;
    private XMLGregorianCalendar linkingDate;
    private String position;
    private double salary;
    private RangeDate linkingTime;
    private RangeDate currentAge;

    public RangeDate getLinkingTime() {
        return linkingTime;
    }

    public void setLinkingTime(RangeDate linkingTime) {
        this.linkingTime = linkingTime;
    }

    public RangeDate getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(RangeDate currentAge) {
        this.currentAge = currentAge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(XMLGregorianCalendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public XMLGregorianCalendar getLinkingDate() {
        return linkingDate;
    }

    public void setLinkingDate(XMLGregorianCalendar linkingDate) {
        this.linkingDate = linkingDate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
