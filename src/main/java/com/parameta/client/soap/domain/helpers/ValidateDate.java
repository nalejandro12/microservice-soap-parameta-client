package com.parameta.client.soap.domain.helpers;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.Period;

public class ValidateDate {
    public static Period rangeDate(XMLGregorianCalendar date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();
        LocalDate initialDate = LocalDate.of(year, month, day);
        LocalDate now = LocalDate.now();

        Period period = Period.between(initialDate, now);

        return period;
    }
}
