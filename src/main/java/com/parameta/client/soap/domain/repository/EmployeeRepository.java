package com.parameta.client.soap.domain.repository;

import com.parameta.client.soap.parameta.AddEmployeeRequest;
import com.parameta.client.soap.parameta.ServiceStatus;

public interface EmployeeRepository {
    ServiceStatus addEmployee(AddEmployeeRequest request);
}
