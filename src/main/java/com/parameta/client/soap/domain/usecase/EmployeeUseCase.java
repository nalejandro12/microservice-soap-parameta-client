package com.parameta.client.soap.domain.usecase;

import com.parameta.client.soap.domain.Employee;
import com.parameta.client.soap.domain.RangeDate;
import com.parameta.client.soap.domain.helpers.ValidateDate;
import com.parameta.client.soap.domain.repository.EmployeeRepository;
import com.parameta.client.soap.parameta.AddEmployeeRequest;
import com.parameta.client.soap.parameta.EmployeeInfo;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeFactory;
import java.time.Period;
import java.util.List;

@Component
public class EmployeeUseCase {

    private final EmployeeRepository employeeRepository;

    public EmployeeUseCase(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee execute(List data) {
        AddEmployeeRequest addEmployeeRequest = mapAddEmployeeRequest(data);
        employeeRepository.addEmployee(addEmployeeRequest);
        Period currentAge = ValidateDate.rangeDate(addEmployeeRequest.getEmployeeInfo().getDateOfBirth());
        Period linkingTime = ValidateDate.rangeDate(addEmployeeRequest.getEmployeeInfo().getLinkingDate());

        return mapEmployee(addEmployeeRequest.getEmployeeInfo(),currentAge,linkingTime);
    }

    private Employee mapEmployee(EmployeeInfo employeeInfo, Period currentAge, Period linkingTime){
        Employee employee = new Employee();
        employee.setName(employeeInfo.getName());
        employee.setLastname(employeeInfo.getLastname());
        employee.setDocumentType(employeeInfo.getDocumentType());
        employee.setDocumentNumber(employeeInfo.getDocumentNumber());
        employee.setDateOfBirth(employeeInfo.getDateOfBirth());
        employee.setLinkingDate(employeeInfo.getLinkingDate());
        employee.setPosition(employeeInfo.getPosition());
        employee.setSalary(employeeInfo.getSalary());
        RangeDate rangeDate = new RangeDate();
        rangeDate.setYears(currentAge.getYears());
        rangeDate.setMonths(currentAge.getMonths());
        rangeDate.setDays(currentAge.getDays());
        employee.setCurrentAge(rangeDate);
        RangeDate rangeDateLinkingTime = new RangeDate();
        rangeDateLinkingTime.setYears(linkingTime.getYears());
        rangeDateLinkingTime.setMonths(linkingTime.getMonths());
        rangeDateLinkingTime.setDays(linkingTime.getDays());
        employee.setLinkingTime(rangeDateLinkingTime);

        return employee;
    }

    private AddEmployeeRequest mapAddEmployeeRequest(List request) {
        AddEmployeeRequest addEmployeeRequest = new AddEmployeeRequest();

        try {
            EmployeeInfo employeeInfo = new EmployeeInfo();
            employeeInfo.setName((String) request.get(0));
            employeeInfo.setLastname((String) request.get(1));
            employeeInfo.setDocumentType((String) request.get(2));
            employeeInfo.setDocumentNumber((String) request.get(3));
            employeeInfo.setDateOfBirth(DatatypeFactory.newInstance().newXMLGregorianCalendar((String) request.get(4)));
            employeeInfo.setLinkingDate(DatatypeFactory.newInstance().newXMLGregorianCalendar((String) request.get(5)));
            employeeInfo.setPosition((String) request.get(6));
            employeeInfo.setSalary((Double) request.get(7));
            addEmployeeRequest.setEmployeeInfo(employeeInfo);
        } catch (Exception exception) {
            System.err.println("Something went wrong.");
        }

        return addEmployeeRequest;
    }
}
