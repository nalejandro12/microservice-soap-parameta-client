package com.parameta.client.soap.infraestructure.client;

import com.parameta.client.soap.domain.repository.EmployeeRepository;
import com.parameta.client.soap.parameta.AddEmployeeRequest;
import com.parameta.client.soap.parameta.AddEmployeeResponse;
import com.parameta.client.soap.parameta.ServiceStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

@Service
public class SoapClient implements EmployeeRepository {

	@Autowired
	private Jaxb2Marshaller marshaller;

	private WebServiceTemplate template;

	private static final String URI = "http://localhost:2026/parametaService";

	@Override
	public ServiceStatus addEmployee(AddEmployeeRequest request) {
		template = new WebServiceTemplate(marshaller);
		AddEmployeeResponse addEmployeeResponse = (AddEmployeeResponse) template.marshalSendAndReceive(URI,
				request);

		return addEmployeeResponse.getServiceStatus();
	}

}
