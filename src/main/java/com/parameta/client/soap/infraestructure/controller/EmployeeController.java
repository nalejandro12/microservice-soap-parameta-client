package com.parameta.client.soap.infraestructure.controller;

import com.parameta.client.soap.domain.helpers.ValidateDate;
import com.parameta.client.soap.domain.usecase.EmployeeUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("api")
public class EmployeeController {

    @Autowired
    private EmployeeUseCase useCase;

    @GetMapping("/employee")
    public ResponseEntity invokeSoapClientToAddEmployee(@RequestParam("nombre") String name,
                                                        @RequestParam("apellido") String lastname,
                                                        @RequestParam("tipoDocumento") String documentType,
                                                        @RequestParam("numeroDocumento") String documentNumber,
                                                        @RequestParam("fechaNacimiento") String dateOfBirth,
                                                        @RequestParam("fechaVinculacion") String linkingDate,
                                                        @RequestParam("cargo") String position,
                                                        @RequestParam("salario") Double salary) throws DatatypeConfigurationException {

        List data = Arrays.asList(name,
                lastname,
                documentType,
                documentNumber,
                dateOfBirth,
                linkingDate,
                position,
                salary);

        String responseIsEmpty = isEmpty(data);

        if(!responseIsEmpty.isEmpty())
            return new ResponseEntity(responseIsEmpty, HttpStatus.BAD_REQUEST);

        String responseIsDate = isDate(dateOfBirth,linkingDate);
        if(!responseIsDate.isEmpty())
            return new ResponseEntity(responseIsDate, HttpStatus.BAD_REQUEST);

        int age = ValidateDate.rangeDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(dateOfBirth)).getYears();

        if(age < 18)
            return new ResponseEntity("El empleado es menor de edad", HttpStatus.BAD_REQUEST);

        return new ResponseEntity(useCase.execute(data), HttpStatus.OK);
    }

    public static String isEmpty(List data){
        if(((String) data.get(0)).isEmpty())
            return "El nombre esta vacío";

        if(((String) data.get(1)).isEmpty())
            return "El apellido esta vacío";

        if(((String) data.get(2)).isEmpty())
            return "El tipoDocumento esta vacío";

        if(((String) data.get(3)).isEmpty())
            return "El numeroDocumento esta vacío";

        if(((String) data.get(4)).isEmpty())
            return "La fechaNacimiento esta vacío";

        if(((String) data.get(5)).isEmpty())
            return "La fechaVinculacion esta vacío";

        if(((String) data.get(6)).isEmpty())
            return "El cargo esta vacío";

        if(((Double) data.get(7)) == 0)
            return "El salario esta vacío";

        return "";
    }

    public static String isDate(String dateOfBirth, String linkingDate){
        if(!isDateValid(dateOfBirth))
            return "El formato de la fecha de cumpleaños es invalido";

        if(!isDateValid(linkingDate))
            return "El formato de la fecha de ingreso es invalido";

        return "";
    }

    public static boolean isDateValid(String date)
    {
        return date.matches("\\d{4}-\\d{2}-\\d{2}");
    }


}
